import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store/store';
import Navbar from './Components/Navbar';
import Aside from './Components/Aside';
import Main from './Components/Main';
import './App.css';

function App() {
  return (
    <Provider store={store} >
      <Router>

        <Navbar />
        <div className="outer-body">
          <Aside />
          <Main />
        </div>

        <Switch>

          <Route exact path="/">
          
          </Route>

        </Switch>

      </Router>
    </Provider>
  );
}

export default App;
