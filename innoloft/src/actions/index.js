export const setData = (data) => {
    return {
        type: "SET_MAIN",
        payload: data,
    }
}

export const setAdditional = (data) => {
    return {
        type: "SET_ADDITIONAL",
        payload: data,
    }
}