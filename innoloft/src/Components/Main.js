import React, { useState } from 'react';
import MainTab from './MainTab';
import AdditionalTab from './AdditionalTab';

export default function Main () {
    const [ selected, setSelected ] = useState('main');

    const handleClick = (e) => {
        console.log(e.target.dataset.name);
        setSelected(e.target.dataset.name);
    }

    return (
        <div className="main">
            <div className="tabs">
                <div
                    data-name="main"
                    onClick={handleClick}
                    className={`${selected === "main" ? 'selected' : null} tab`}>
                        Main Information
                </div>
                <div
                    data-name="additional"
                    onClick={handleClick}
                    className={`${selected === "additional" ? 'selected' : null} tab`}>
                        Additional Information
                </div>
            </div>
            {selected === "main" ? <MainTab /> : <AdditionalTab />}
        </div>
    )
}