import React from 'react';


export default function Navbar() {
    return (
        <nav>
            <ul className="navul">
                <li className="nav-logo">
                    <div className="logo">
                        ENER<span className="nyellow">G</span>IE
                        <span className="bordered">LOFT</span>
                    </div>
                </li>
                <li>
                    <a className="nav-item" href="#notifications">
                        <i className="material-icons">notifications</i>
                    </a>
                </li>
                <li>
                    <a className="nav-item" href="#mail">
                        <i className="material-icons">mail</i>
                    </a>
                </li>
                <li>
                    <a className="nav-item" href="#public">
                        <i className="material-icons">public</i><span className="public-text">EN</span>
                    </a>
                </li>
            </ul>
        </nav>
    )
}