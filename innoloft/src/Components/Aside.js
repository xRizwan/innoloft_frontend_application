import React from 'react';

export default function Aside() {

    return (
        <aside className="sidenav">
            <ul className="asideul">
                <li>
                    <a href="#home">
                        <i className="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                <li>
                    <a href="#account">
                        <i className="material-icons">account_circle</i>
                        <span>My Account</span>
                    </a>
                </li>
                <li>
                    <a href="#company">
                        <i className="material-icons">corporate_fare</i>
                        <span>My Company</span>
                    </a>
                </li>
                <li>
                    <a href="#settings">
                        <i className="material-icons">settings</i>
                        <span>My Settings</span>
                    </a>
                </li>
                <li>
                    <a href="#news">
                        <i className="material-icons">pages</i>
                        <span>News</span>
                    </a>
                </li>
                <li>
                    <a href="#analytics">
                        <i className="material-icons">analytics</i>
                        <span>Analytics</span>
                    </a>
                </li>
            </ul>
        </aside>
    )
}