import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Error from './Error';
import { setAdditional } from '../actions/';

export default function AdditionalTab() {

    const dispatcher = useDispatch();
    const data = useSelector(state => state.additional);

    const [ firstName, setFirstName ] = useState(data.firstName);
    const [ lastName, setLastName ] = useState(data.lastName);
    const [ address, setAddress ] = useState(data.address);
    const [ country, setCountry ] = useState(data.country);
    const [ message, setMessage ] = useState('');

    const handleSubmit = (e) => {
        dispatcher(setAdditional({
            firstName,
            lastName,
            address,
            country,
        }))
        setMessage('Success');
    }

    return (
        <div className="additionaltab">
            <Error message={message} success={true} />
            <div className="input-container">
                <div className="input-label">First Name:</div> 
                <input
                    className="input-field"
                    type="text"
                    placeholder="first name"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                />
            </div>
            <div className="input-container">
                <div className="input-label">Last Name:</div> 
                <input
                    className="input-field"
                    type="text"
                    placeholder="last name"
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                />
            </div>
            <div className="input-container">
                <div className="input-label">Country:</div> 
                <select
                    name="countries"
                    className="countryselect" 
                    value={country}
                    onChange={e => setCountry(e.target.value)}
                >
                    <option value="germany">Germany</option>
                    <option value="austria">Austria</option>
                    <option value="switzerland">Switzerland</option>
                </select>
            </div>
            <div className="input-container">
                <div className="input-label">Address:</div> 
                <input
                    className="input-field"
                    type="text"
                    placeholder="address"
                    value={address}
                    onChange={e => setAddress(e.target.value)}
                />
            </div>
            <button onClick={handleSubmit} className="submitbtn">Submit</button>
        </div>
    )
}