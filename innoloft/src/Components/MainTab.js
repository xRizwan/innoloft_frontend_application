import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import Error from './Error';
import { setData } from '../actions/';

export default function MainTab() {
    let data = useSelector(state => state.main)

    const [ email, setEmail ] = useState(data.email);
    const [ password, setPassword ] = useState(data.password);
    const [ confirm, setConfirm ] = useState('');
    const [ message, setMessage ] = useState('');
    const [ error, setError ] = useState('');
    const [ passError, setPassError ] = useState(false);

    const dispatcher = useDispatch();

    const handleSubmit = (e) => {
        setMessage('');
        setError('');
        setPassError(false);

        let emailregex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        let test = emailregex.test(email)

        if(!test) {
            setError('Email address is not valid');
            return;
        }

        if (password.length < 8) {
            setPassError(true);
            setError('Password is too weak');
            return;
        } else if (password !== confirm) {
            setPassError(true);
            setError('Passwords do not match')
            return;
        }

        dispatcher(setData({email, password}))

        setMessage('Saved');
    }

    const handlePassChange = (e) => {
        if (e.target.value.length < 8) {
            setPassError(true);
            setError('Password is too weak');
        } else {
            setPassError(false);
            setError('');
        }
        setPassword(e.target.value);
    }

    const handleConfirmChange = (e) => {
        if (password !== e.target.value) {
            setError('Passwords Do not Match');
        } else {
            setError('');
        }

        setConfirm(e.target.value);
    }

    return (
        <div className="maintab">
            <Error message={error} />
            <Error message={message} success={true} />
            <div className="input-container">
                <div className="input-label">Email:</div> 
                <input
                    className="input-field"
                    type="text"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
            </div>
            <div className="input-container">
                <div className="input-label">Password:</div> 
                <input
                    className={`input-field ${passError ? 'pError' : null}`}
                    type="password"
                    placeholder="password"
                    value={password}
                    onChange={handlePassChange}
                />
            </div>
            <div className="input-container">
                <div className="input-label">Confirm Password:</div> 
                <input
                    className="input-field"
                    type="password"
                    placeholder="confirm password"
                    value={confirm}
                    onChange={handleConfirmChange}
                />
            </div>
            <button onClick={handleSubmit} className="submitbtn">Submit</button>
        </div>
    )
}