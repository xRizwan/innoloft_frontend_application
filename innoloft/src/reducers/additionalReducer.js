const additionalReducer = (state = {firstName: '', lastName: '', country: '', city: '', address: ''}, action) => {
    switch(action.type) {
        case 'SET_ADDITIONAL':
            return {
                firstName: action.payload.firstName,
                lastName: action.payload.lastName,
                country: action.payload.country,
                city: action.payload.city,
                address: action.payload.address
            }
        default:
            return state
    }
}

export default additionalReducer;