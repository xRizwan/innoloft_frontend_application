import { combineReducers } from 'redux';
import mainReducer from './mainReducer';
import additionalReducer from './additionalReducer';

const allReducers = combineReducers({
    main: mainReducer,
    additional: additionalReducer,
})

export default allReducers;