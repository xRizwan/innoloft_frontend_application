const mainReducer = (state = {email: '', password: ''}, action) => {
    switch(action.type){
        case "SET_MAIN":
            return {email: action.payload.email, password: action.payload.password};
        default:
            return state;
    }
}

export default mainReducer;